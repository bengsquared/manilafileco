import React from "react"
import logo from "../img/manila.svg"

export default function Footer() {
  return (
    <footer>
      <ul id="address">
        <li>
          <h4>Manila File Co.</h4>
        </li>
        <li>
          <h4>Made in California</h4>
        </li>
      </ul>
      <ul id="aboutcontact">
        <li>
          <a href="https://bengraneygreen.com/Manila-File-Company-67f3f89ef2984f07af3675af88460f2e">
            <h4>About</h4>
          </a>
        </li>
        <li>
          <a href="mailto:bengg@mac.com">
            <h4>Contact</h4>
          </a>
        </li>
        <li>
          <a href="https://www.producthunt.com/upcoming/manilla">
            <h4>Subscribe</h4>
          </a>
        </li>
      </ul>
    </footer>
  )
}
