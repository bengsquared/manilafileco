import React from "react"
import logo from "../img/manila.svg"

export default function Header({ currentPage }) {
  return (
    <header>
      <a href="/" className="header-title">
        <img alt="Manila logo" className="header-icon" src={logo} />
        <h4>Manila File Co</h4>
      </a>
      <nav className="header-nav">
        <a href="https://bengraneygreen.com/Manila-File-Company-67f3f89ef2984f07af3675af88460f2e">
          <h4>About</h4>
        </a>
        <a href="mailto:bengg@mac.com">
          <h4>Contact</h4>
        </a>
      </nav>
    </header>
  )
}
