import React from "react"
import Layout from "../components/layout"
import logo from "../img/manila.svg"
import meta from "../img/tidy-up-meta.png"
import Helmet from "react-helmet"
import tidyUp from "../img/tidyup.svg"
import tidyUpButtons from "../img/tidyUpButtons.png"
import Header from "../components/header"
import Footer from "../components/footer"

export default function Home() {
  return (
    <Layout className="wrap">
      <Header currentPage="home" />
      <Helmet>
        <title>tidyUp: swipe through your filesystem</title>
        <meta name="title" content="tidyUp: swipe through your filesystem" />
        <meta
          name="description"
          content="tidyUp is a simple app for sorting through large and messy folders on your computer"
        />

        <meta property="og:type" content="website" />
        <meta property="og:url" content="https://manilafile.co/tidyup/" />
        <meta
          property="og:title"
          content="tidyUp: swipe through your filesystem"
        />
        <meta
          property="og:description"
          content="tidyUp is a simple app for sorting through large and messy folders on your computer"
        />
        <meta property="og:image" content={meta} />

        <meta property="twitter:card" content="summary_large_image" />
        <meta property="twitter:url" content="https://manilafile.co/tidyup/" />
        <meta
          property="twitter:title"
          content="tidyUp: swipe through your filesystem"
        />
        <meta
          property="twitter:description"
          content="tidyUp is a simple app for sorting through large and messy folders on your computer"
        />
        <meta property="twitter:image" content={meta} />
      </Helmet>
      <main>
        <section className="splash">
          <img alt="tidyUp logo" src={tidyUp} className="BigImage center" />
          <h1>tidyUp</h1>
          <h2>helps you clean up your desktop and other large folders</h2>
          <p className="centerText"> Mac Beta available now! </p>
          <div className="flex justify-center">
            <a className="signUpButton" href={"../../tidyUp.dmg"} download>
              Download for Mac
            </a>
          </div>
        </section>
        <section className="body-section">
          <p>
            tidyUp lets you sort through any clutter you have on your desktop.
            Just choose a folder to cleanup, and swipe through your files using
            your arrow keys!
          </p>
          <br />
          <img src={tidyUpButtons} className="BigImage center" />
          <p>
            Just a few swipes, and you've Tidied Up
            <span role="img" aria-label="sparkles">
              ✨
            </span>
          </p>
        </section>
      </main>
      <Footer />
    </Layout>
  )
}
